function flatonacci(signature,n){
  
  if(Array.isArray(signature) && Number.isInteger(n) ){
    n = Math.abs(n)
    
    if(signature.length < 3){
      return 'not valid signature';
    }else if(n == 0){
    	return [];
    }else if(n > 0 && n < 4){
    	return signature;
    }else{
    	for(i=0; i < signature.length; i++){
          signature[i] = parseInt(signature[i]);
        }

      	for(j=0; j < n; j++){	
          	if(signature.length == n) break;
            signature.push(signature[j] + signature[j+1] + signature[j+2]);
        }
      
      	return signature;
    }
    
  }else{
    return 'not valid input';
  }
  
}


signature = [1,'2',3];
n = 4;
console.log(flatonacci(signature,n));
