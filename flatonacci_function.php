<?php

function flatonacci($signature,$n){

	if(is_array($signature) && is_numeric($n)){

		$n = abs(intval($n));
		
		if(count($signature) < 3){
			
			return 'not valid signature';
			
		}else if($n == 0){
			
			return [];
			
		}else if($n > 0 && $n < 4){
			
			return $signature;
			
		}else{
			
			for($i=0; $i<count($signature); $i++){
				$signature[$i] = intval($signature[$i]);
			}
			
			for($j=0; $j<$n; $j++){
				
				if(count($signature) == $n){
				    break;
				}
				
				$signature[] = $signature[$j] + $signature[$j+1] + $signature[$j+2];
			}
			
			
			return $signature;
		
		}
		
		
	}else{
		return 'not valid input';
	}
}



$signature = [1,'5',3];
$n = 4;

print_r(flatonacci($signature,$n));
