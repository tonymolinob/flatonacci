#-*- coding:utf-8 -*-

def flatonacci(signature,n):
      
    if type(signature) == list and type(n) == int :
        
        n = int(abs(n))
        
        if len(signature) < 3:
            
            return 'not valid signature'
        
        elif n == 0:
            signature = []
            return signature
        
        elif n > 0 and n < 4:
            return signature   
        else:
            
            signature = [int(x) for x in signature]
            
            for i in range(n):
                
                if len(signature) == n:
                    break
                    
                signature.append(signature[i]+ signature[i+1] + signature[i+2])
                
            return signature
       
    else:
        return 'not valid input'


if __name__ == '__main__':
    signature = [1,'5',3]
    n = 4
    print(flatonacci(signature,n))
